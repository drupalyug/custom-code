Часто попадаются задачи, когда нужно вертикально выровнять текст в блоке, вне зависимости от того, сколько там строк в тексте.

![01_1.png](https://bitbucket.org/repo/KErb7z/images/1711371494-01_1.png)

Конечно есть решение с vertical-align: middle + display: table-cell - но не всегда удобно использовать данный подход. Недавно наткунлся на интересное решение, и немного переделал его.
Суть в следующем Если в блоке, где нужно вертикальное выравнивание (parent) вывести блок с высотой 100% и display: inline-block; то другие inline-block элементы будут так же выравниваться относительно нашего элемента. На схеме применрно показана логика выполнения.

![02_0.png](https://bitbucket.org/repo/KErb7z/images/3202113548-02_0.png)

Попробую объяснить подробнее.
**Parent** - это наш родительскйи блок <div class="parent"></div>
**Children** - блок, который нужно вертикально выровнять <div class="children"></div>
**Helper** - доп. *невидимый* блок, которому нужно выставить 100% высоту и vertical-align: middle; После чего Children будет вертикально выравниваться относительно helper-а
 
В решении был следующий код HTML разметки


    <div class="parent">
        <div class="children">Текст который нужно вертикально центрировать</div>
        <div class="helper"></div>
    </div>
    css код для данной разметки
    .parent{
      background: #ffa none repeat scroll 0 0;
      border: 1px solid #dd0;
      height: 186px;
      margin: 10px;
      text-align: center;
      width: 500px;
    }
    .helper{
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        width: 0;
    }
    .children{
        background: #ddf none repeat scroll 0 0;
        border: 1px solid #080;
        display: inline-block;
        vertical-align: middle;
        width: 300px;
    }

Но немного подумав, я отказался от helper-а и использовал псевдоэлемент ::after  - вот что получилось

    <div class="parent">
        <div class="children">Текст который нужно вертикально центрировать</div>
    </div>
    Ну и конечно css
    .parent{
      background: #ffa none repeat scroll 0 0;
      border: 1px solid #dd0;
      height: 186px;
      margin: 10px;
      text-align: center;
      width: 500px;
    }
    
    .parent::after {
        content: "";
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        width: 0;
    }
    .children{
        background: #ddf none repeat scroll 0 0;
        border: 1px solid #080;
        display: inline-block;
        vertical-align: middle;
        width: 300px;
    }

Берем, меняем длину текста... радуемся и  пользуемся на здровье...